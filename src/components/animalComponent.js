import { Component } from "react";
import ImageCat from "../assets/images/cat.jpg";

class Animal extends Component {
    render() {
        let { name } = this.props
        return (
            <div>
                {name === "cat" ? <img src={ImageCat} alt="cat" width="500px"></img> : <p>meow not found</p>}
            </div>
        )
    }
}

export default Animal