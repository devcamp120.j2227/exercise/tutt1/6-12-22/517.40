import './App.css';
import Animal from './components/animalComponent';

function App() {
  return (
    <div >
      <Animal name="cat"/>
    </div>
  );
}

export default App;
